#![feature(plugin)]
#![plugin(rocket_codegen)]
extern crate rocket;

#[macro_use]
extern crate log;
extern crate env_logger;
extern crate num;

use std::collections::HashMap;
use std::sync::RwLock;

use rocket::response::status::BadRequest;
use rocket::State;

use num::bigint::BigUint;
use num::integer::Integer;
use num::pow;
use num::traits::{One, Zero};

struct ServerData {
    limit: u64,
    cache: RwLock<HashMap<u64, BigUint>>,
}

fn fibonacci(cache: &RwLock<HashMap<u64, BigUint>>, i: u64) -> BigUint {
    if let Ok(cache) = cache.read() {
        if let Some(res) = cache.get(&i) {
            debug!("Cache hit at index {}", i);
            return res.clone();
        }
    }

    let k = i >> 1;
    let r = if i.is_odd() {
        pow(fibonacci(cache, k + 1), 2) + pow(fibonacci(cache, k), 2)
    } else {
        (fibonacci(cache, k + 1) + fibonacci(cache, k - 1)) * fibonacci(cache, k)
    };

    if let Ok(mut cache) = cache.write() {
        debug!("Cache store at index {}", i);
        cache.insert(i, r.clone());
    }

    r
}

fn handle(n: u64, data: State<ServerData>) -> Result<BigUint, BadRequest<&'static str>> {
    if n > data.limit {
        Err(BadRequest(Some("Limit exceeded")))
    } else {
        Ok(fibonacci(&data.cache, n))
    }
}

#[get("/<n>")]
fn dec(n: u64, data: State<ServerData>) -> Result<String, BadRequest<&'static str>> {
    handle(n, data).map(|f| format!("{}", f))
}

#[get("/x/<n>")]
fn hex(n: u64, data: State<ServerData>) -> Result<String, BadRequest<&'static str>> {
    handle(n, data).map(|f| format!("{:x}", f))
}

#[get("/o/<n>")]
fn oct(n: u64, data: State<ServerData>) -> Result<String, BadRequest<&'static str>> {
    handle(n, data).map(|f| format!("{:o}", f))
}

#[get("/b/<n>")]
fn bin(n: u64, data: State<ServerData>) -> Result<String, BadRequest<&'static str>> {
    handle(n, data).map(|f| format!("{:b}", f))
}

#[get("/bytes_le/<n>")]
fn bytes_le(n: u64, data: State<ServerData>) -> Result<Vec<u8>, BadRequest<&'static str>> {
    handle(n, data).map(|f| f.to_bytes_le())
}

#[get("/bytes_be/<n>")]
fn bytes_be(n: u64, data: State<ServerData>) -> Result<Vec<u8>, BadRequest<&'static str>> {
    handle(n, data).map(|f| f.to_bytes_be())
}

fn main() {
    env_logger::init();

    let limit = std::env::var("FIBIG_LIMIT")
        .unwrap_or_default()
        .parse::<u64>()
        .unwrap_or(u64::max_value());

    let cache = [
        (0, BigUint::zero()),
        (1, BigUint::one()),
        (2, BigUint::one()),
    ].iter()
        .cloned()
        .collect();

    let cache = RwLock::new(cache);

    rocket::ignite()
        .manage(ServerData { limit, cache })
        .mount("/", routes![dec, hex, bin, oct, bytes_le, bytes_be])
        .launch();
}
