# fibig
Big Fibonacci Calculator

[Example API server here](https://fibig.petoknm.tk/1000) (Limit=1e7)

## Endpoints
 - `/<n>` - decimal output
 - `/x/<n>` - hexadecimal output
 - `/o/<n>` - octal output
 - `/b/<n>` - binary output (zeros and ones, not raw byte output)
 - `/bytes_le/<n>` - raw bytes, little endian
 - `/bytes_be/<n>` - raw bytes, big endian

where `<n>` is a `u64` number.

## Configuration
 - environment variable `FIBIG_LIMIT` sets the upper limit of `<n>`
