FROM clux/muslrust:nightly as builder
COPY . /volume/
RUN cargo build --release

FROM scratch
COPY --from=builder /volume/target/x86_64-unknown-linux-musl/release/fibig /
EXPOSE 80
CMD ["/fibig"]

